package com.example.exam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private calculo Calculo;
    private EditText txtCliente;
    private Button btnIr;
    private Button btnSalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtCliente = (EditText)findViewById(R.id.txtCliente);
        btnIr=(Button)findViewById(R.id.btnIr);
        btnSalir=(Button)findViewById(R.id.btnSalir);

        Calculo = new calculo();
        btnIr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cliente = txtCliente.getText().toString();

                if(cliente.matches("")){
                    Toast.makeText(MainActivity.this,"Falto capturar el nombre", Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent intent = new Intent(MainActivity.this,calculoActivity.class);
                    //Enviar un dato String
                    intent.putExtra("cliente",cliente);

                    //Enviar un objeto
                    Bundle objeto = new Bundle();
                    objeto.putSerializable("cotizacion",Calculo);
                    intent.putExtras(objeto);
                    startActivity(intent);
                }
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
