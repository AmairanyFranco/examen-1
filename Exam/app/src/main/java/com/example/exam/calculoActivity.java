package com.example.exam;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import static com.example.exam.calculo.*;

public class calculoActivity extends AppCompatActivity {
    private calculo Calculo;
    private EditText txtrecibo;
   private EditText txtname;
    private Button btnCal;
    private Button btnLimpiar;
    private Button btnCerrar;
    private EditText txthoras;
    private EditText txttrabajadas;
    private RadioGroup rGroup;
    private RadioButton rA;
    private RadioButton rAl;
    private RadioButton rO;
    private TextView lblsub;
    private EditText txtSubtotal;
    private TextView lblimpu;
    private EditText txtimpuesto;
    private TextView lbltotal;
    private EditText txtTotal;
    private EditText txtextras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculo);
        txtrecibo= findViewById(R.id.txtrecibo);
        btnCal= findViewById(R.id.btnCal);
        btnLimpiar= findViewById(R.id.btnLimpiar);
        btnCerrar= findViewById(R.id.btnCerrar);
        txtname= findViewById(R.id.txtname);
        txthoras= findViewById(R.id.txthoras);
        txtextras= findViewById(R.id.txtextras);
        rGroup= findViewById(R.id.rGroup);
        rA= findViewById(R.id.rA);
        rAl= findViewById(R.id.rAl);
        rO= findViewById(R.id.rO);
        lblsub= findViewById(R.id.lblsub);
        lblimpu= findViewById(R.id.lblImpu);
        lbltotal= findViewById(R.id.lbltotal);

        Bundle datos = getIntent().getExtras();

        txtname.setText("Nombre del trabajador: "+datos.getString("cliente"));

        Calculo = (calculo) datos.getSerializable("calculo");
        btnCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sub = txtSubtotal.getText().toString();
                String impu = txtimpuesto.getText().toString();
                String total = txtTotal.getText().toString();
                if (sub.matches("") || impu.matches("") || total.matches("")) {
                    Toast.makeText(calculoActivity.this, "Falto capturar algun dato", Toast.LENGTH_LONG).show();
                } else {

                    calculo.setSubtotal(Float.valueOf(txtSubtotal.getText().toString()));
                    calculo.setImpuesto(Float.valueOf(txtimpuesto.getText().toString()));
                    calculo.setTotal(Float.valueOf(txtTotal.getText().toString()));





                }



                }

            }
        }
    }
