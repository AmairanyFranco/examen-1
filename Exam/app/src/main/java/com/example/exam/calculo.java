package com.example.exam;

import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;

public class calculo implements Serializable {
    private int recibo;
    private String nombre;
    private float htrabajadas;
    private float hextras;
    private float subtotal;
    private float impuesto;
    private int totalpagar;

    public calculo(int recibo, String nombre, float htrabajadas, float hextras, float subtotal, float impuesto, int totalpagar){
        this.recibo =recibo;
        this.nombre = nombre;
        this.htrabajadas = htrabajadas;
        this.hextras = hextras;
        this.subtotal = subtotal;
        this.impuesto = impuesto;
        this.totalpagar = totalpagar;
    }

    public static void setTotal(Float valueOf) {
    }

    public int getRecibo() {
        return recibo;
    }

    public String getNombre() {
        return nombre;
    }

    public float getHtrabajadas() {
        return htrabajadas;
    }

    public float getHextras() {
        return hextras;
    }

    public float getSubtotal() {
        return subtotal;
    }

    public float getImpuesto() {
        return impuesto;
    }

    public int getTotalpagar() {
        return totalpagar;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setRecibo(int recibo) {
        this.recibo = recibo;
    }

    public void setHtrabajadas(float htrabajadas) {
        this.htrabajadas = htrabajadas;
    }

    public void setHextras(float hextras) {
        this.hextras = hextras;
    }

    public static void setImpuesto(float impuesto) {

    }

    public static void setSubtotal(float subtotal) {

    }

    public void setTotalpagar(int totalpagar) {
        this.totalpagar = totalpagar;
    }


}
